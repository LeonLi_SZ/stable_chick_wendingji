def on_forever():
    servos.P1.set_angle(Math.constrain(pins.map(input.rotation(Rotation.ROLL), -90, 90, 0, 180),
            0,
            180))
basic.forever(on_forever)
